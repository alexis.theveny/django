from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
import requests
import base64
from bs4 import BeautifulSoup
from urllib.parse import quote, unquote

from django.views.decorators.http import require_http_methods

# import logging
# logger = logging.getLogger(__name__)
from .models import Prospect, Visit


def index(request):
    # return HttpResponse(f"Hi")
    return render(request, "index.html")


def compute_square(request, number):
    square = number * number
    context = {"square": square, "number": number}
    return render(request, "compute_square.html", context=context)


def compute_squares(request, number):
    numbers = list(range(number))
    squares = [n**2 for n in numbers]
    context = {
        "number_and_square": [
            {"number": number, "square": square}
            for number, square in zip(numbers, squares)
        ]
    }
    return render(request, "compute_squares.html", context=context)


def random_wiki(request):
    url = "https://en.wikipedia.org/wiki/Special:RandomInCategory/Featured_articles"
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")

    lang = ", ".join(
        [div.text.strip() for div in soup.find_all(class_="interlanguage-link")]
    )

    title = soup.find(id="firstHeading").text

    context = {"nom": title, "langues": lang}
    return render(request, "random_wiki.html", context=context)


def send_image(request, data_encoded):
    data = unquote(data_encoded)
    with open("test.txt", "w") as file:
        file.write(data)
    context = {"data": data}
    return render(request, "send_image.html", context=context)


@require_http_methods(["GET", "POST"])
def form_prospect(request):
    if request.method == "POST":
        first_name = request.POST.get("firstname")
        last_name = request.POST.get("lastname")
        tel = request.POST.get("tel")
        email = request.POST.get("email")
        message = request.POST.get("message")
        prospect_object = Prospect.objects.create(
            first_name=first_name,
            last_name=last_name,
            tel=tel,
            email=email,
            message=message,
        )
        prospect_object.save()

        # logger.debug(
        #     " - ".join(
        #         [
        #             "[FORM ]",
        #             f"first_name: f{first_name}",
        #             f"last_name: f{last_name}",
        #             f"tel: f{tel}",
        #             f"email: f{email}",
        #         ]
        #     )
        # )
        return render(request, "form_received.html")
    return render(request, "form_prospect.html", context={})


@require_http_methods(["GET", "POST"])
def landing_page(request):
    visit_object = Visit.objects.create()
    visit_object.save()
    return render(request, "landing_page.html", context={})
