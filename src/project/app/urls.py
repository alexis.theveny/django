from django.contrib import admin
from django.urls import path

from .views import *

urlpatterns = [
    path("", index, name="index"),
    path("compute_square/<int:number>", compute_square, name="compute_square"),
    path("compute_squares/<int:number>", compute_squares, name="compute_squares"),
    path("random_wiki/", random_wiki, name="random_wiki"),
    path("send_image/<str:data_encoded>", send_image, name="send_image"),
    path("form_prospect", form_prospect, name="form_prospect"),
    path("landing_page", landing_page, name="landing_page"),
]
