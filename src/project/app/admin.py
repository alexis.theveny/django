from django.contrib import admin

# Register your models here.
from .models import Prospect, Visit

class ProspectAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "last_name",
        "first_name",
        "tel",
        "email",
        "created",
        "updated",
    )
    readonly_fields = (
        "id",
        "last_name",
        "first_name",
        "tel",
        "email",
        "created",
        "updated",
        "message",
    )
admin.site.register(Prospect, ProspectAdmin)

class VisitAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "visit_time",
    )
    readonly_fields = (
        "id",
        "visit_time",
    )
admin.site.register(Visit, VisitAdmin)
